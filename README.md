# Gitlab Portofolio

[![](https://gitlab.com/gitlab-org/gitlab-ee/badges/master/build.svg)](https://wahidari.gitlab.io)
[![](https://semaphoreci.com/api/v1/projects/2f1a5809-418b-4cc2-a1f4-819607579fe7/400484/shields_badge.svg)](https://wahidari.gitlab.io)
[![](https://img.shields.io/badge/docs-latest-brightgreen.svg?style=flat&maxAge=86400)](https://wahidari.gitlab.io)
[![](https://img.shields.io/badge/Find%20Me-%40wahidari-009688.svg?style=social)](https://wahidari.gitlab.io)

## Language

- [![](https://img.shields.io/badge/html-5-FF5722.svg)](https://wahidari.gitlab.io) 
- [![](https://img.shields.io/badge/css-3-03A9F4.svg)](https://wahidari.gitlab.io) 
- [![](https://img.shields.io/badge/javascript-1.8-FFCA28.svg)](https://wahidari.gitlab.io) 

## Test coverage

- [![](https://gitlab.com/gitlab-org/gitlab-ee/badges/master/coverage.svg)](https://wahidari.gitlab.io) html
- [![](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg)](https://wahidari.gitlab.io) css
- [![](https://s3.amazonaws.com/assets.coveralls.io/badges/coveralls_87.svg)](https://wahidari.gitlab.io) js

## License
> This program is Free Software: 
You can use, study, share and improve it at your will. 
Specifically you can redistribute and/or modify it under the terms of the [GNU General Public License](https://www.gnu.org/licenses/gpl.html) 
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
